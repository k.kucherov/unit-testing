const server = require('../server.js');
const path = require('path');
const request = require('request-promise');
const config = require('../config/index.js');
const fs = require('fs-extra');
const assert = require('assert');
const faker = require('faker');
const { COPYFILE_EXCL } = fs.constants;

describe('GET request', () => {
	let app;

	const PORT = 3333;
	const BASE_URI = `http://localhost:${PORT}`;

	beforeAll((done) => {
		app = server.listen(PORT, () => {
			done();
		});
	});
	

	describe('/', () => {
		const indexFile = path.join(config.publicRoot, 'index.html');
		let indexContent;

		beforeEach(() => {
			indexContent = fs.readFileSync(indexFile, {encoding: 'utf-8'});
		})

		it('returns index.html', async () => {
			const response = await request.get(BASE_URI);
			expect(response).toEqual(indexContent);
		})
	})

	describe('/file.txt', () => {
		const filePath = path.join(config.filesRoot, 'file.txt');
		const fileContent = faker.lorem.text();

		beforeAll(() => {
			fs.writeFile(filePath, fileContent);
		});

		it('should return file.txt', async () => {
			const response = await request.get(`${BASE_URI}/file.txt`);
			expect(response).toEqual(fileContent);
		});

		afterAll(() => {
			fs.removeSync(filePath);
		});
	})

	afterAll((done) => {
		app.close(() => {
		done();
		});
	});

});
